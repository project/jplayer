The master branch is no longer used. Please checkout a named branch. For
example:

  $ git checkout 7.x-2.x

